##Creating Deployments
```
kubectl create deployment flatris --image=registry.gitlab.com/v_kostyukov/flatris:latest
```
##Get more details on your updated Deployment
```
kubectl get deploy
kubectl describe deploy
```
##ReplicaSet
```
kubectl get rs
kubectl describe rs
```
##Check pod status
```
kubectl get pod
kubectl get pod -A
kubectl describe pod
```
##Logs
```
kubectl logs flatris
```
##See yaml file
```
kubectl get pod -o yaml flatris
```
##start k8s with yaml file
```
kubectl apply
```
##Create a Service to expose your Deployment
```
kubectl expose deployment flatris --port=3000 --target-port=3000 --type=LoadBalancer
```
##Service
```
kubectl get service
kubectl get svc
```
##Scale a Deployment
```
kubectl scale deployment --replicas=3 flatris
```

